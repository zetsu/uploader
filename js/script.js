function uploadFile() {
      const fileInput = document.getElementById('file');
      const file = fileInput.files[0];

      if (!file) {
        showMessage('Please select a file.');
        return;
      }

      if (file.size > 50 * 1024 * 1024) {
        showMessage('File size exceeds the limit of 50MB.');
        return;
      }

      const formData = new FormData();
      formData.append('file', file);

      const xhr = new XMLHttpRequest();

      xhr.open('POST', 'https://cdn.miraaaa.my.id/upload');

      xhr.upload.onprogress = (event) => {
        const progress = (event.loaded / event.total) * 100;
        updateProgressBar(progress);
      };

      xhr.onload = () => {
        if (xhr.status === 200) {
          const data = JSON.parse(xhr.responseText);
          showMessage(data.creator);
          showFileUrl(data.result.link);
        } else {
          showMessage('An error occurred during file upload.');
        }
      };

      xhr.send(formData);
    }

    function showMessage(message) {
      const messageElement = document.getElementById('message');
      messageElement.textContent = message;
    }

    function showFileUrl(fileUrl) {
      const fileUrlElement = document.getElementById('file-url');
      fileUrlElement.innerHTML = `<a href="${fileUrl}" target="_blank">Direct File URL</a>`;
    }

    function updateProgressBar(progress) {
      const progressBar = document.getElementById('progress');
      progressBar.style.width = `${progress}%`;
    }
