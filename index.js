const express = require('express');
const multer = require('multer');
const path = require('path');

const app = express();
const PORT = 3000;

// Custom middleware to check referrer before serving js/script.js
const checkReferrer = (req, res, next) => {
  const referrer = req.get('referrer') || req.get('referer');

  // Replace 'https://cdn.miraaaa.my.id' with your actual domain name
  if (referrer && referrer.includes('https://cdn.miraaaa.my.id')) {
    next();
  } else {
    res.status(403).send('Forbidden');
  }
};

// Set up Multer to handle file uploads
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './file');
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
    cb(null, uniqueSuffix + path.extname(file.originalname));
  },
});

const upload = multer({ storage: storage });

// Serve uploaded files as static content
app.use('/file', express.static(path.join(__dirname, 'file')));

// Route to handle file file
app.post('/upload', upload.single('file'), (req, res) => {
  res.set('Content-Type', 'application/json');
  if (!req.file) {
    return res.status(400).json({ error: 'No file provided.' });
  }

  const fileUrl = `https://cdn.miraaaa.my.id/file/${req.file.filename}`;

  return res.json({
    creator: "Mira",
    result: {
      link: fileUrl,
    },
  });
});

// Use the checkReferrer middleware for the script.js route
app.get('/js/script.js', checkReferrer, (req, res) => {
  res.sendFile(__dirname + '/js/script.js');
});
app.get('/css/main.css', checkReferrer, (req, res) => {
  res.sendFile(__dirname + '/css/main.css');
});

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on https://cdn.miraaaa.my.id:${PORT}`);
});
